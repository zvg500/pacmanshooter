package ru.vzg.packman.tools;

import ru.vzg.packman.Actors.Bullet;
import ru.vzg.packman.Main;
import ru.vzg.packman.screens.GameSc;

public class BulletGenerator {
    boolean isFire;

    public void update(Joystick joy){
        isFire = (joy.getDir().getX() == 0 && joy.getDir().getY() == 0)?false:true;
        if(isFire) GameSc.bullets.add(new Bullet(Main.bullet,GameSc.player.position,10,GameSc.player.R/2,joy.getDir()));
    }
}
