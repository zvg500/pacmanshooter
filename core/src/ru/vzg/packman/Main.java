package ru.vzg.packman;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import ru.vzg.packman.screens.GameSc;

public class Main extends Game {
	public static SpriteBatch batch;

	public static int WIDTH,HEIGHT;
	public static Texture circle,actor,actor2,bullet,joy;

	
	@Override
	public void create () {
		batch = new SpriteBatch();


		WIDTH = Gdx.graphics.getWidth();
		HEIGHT = Gdx.graphics.getHeight();
		circle = new Texture("ZGUhzRkn0aw.jpg");
		actor = new Texture("Pacman.png");
		actor2 = new Texture("PacmanDown.png");
		bullet = new Texture("Bullet.png");
		joy = new Texture("circle.png");
		setScreen(new GameSc(this));

	}


	
	@Override
	public void dispose () {
		batch.dispose();

	}
}
