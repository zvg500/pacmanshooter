package ru.vzg.packman.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import ru.vzg.packman.Actors.Bullet;
import ru.vzg.packman.Actors.Player;
import ru.vzg.packman.Main;
import ru.vzg.packman.tools.BulletGenerator;
import ru.vzg.packman.tools.Joystick;
import ru.vzg.packman.tools.Point2D;

public class GameSc implements Screen {
    Joystick joy, joy2;
    public static Player player;
    public static Array<Bullet> bullets;
    BulletGenerator bullgen;
    Main main;

    public GameSc(Main main){
        this.main = main;
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(new InputProcessor() {
            @Override
            public boolean keyDown(int keycode) {
                return false;
            }

            @Override
            public boolean keyUp(int keycode) {
                return false;
            }

            @Override
            public boolean keyTyped(char character) {
                return false;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                screenY = Main.HEIGHT - screenY;
                multitouch(screenX,screenY,true,pointer);
                return false;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                screenY = Main.HEIGHT - screenY;
                multitouch(screenX,screenY,false,pointer);
                return false;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                screenY = Main.HEIGHT - screenY;

                multitouch(screenX,screenY,true,pointer);
                return false;
            }

            @Override
            public boolean mouseMoved(int screenX, int screenY) {
                return false;
            }

            @Override
            public boolean scrolled(int amount) {
                return false;
            }
        });
        loadActors();

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        GameUpdate();
        Main.batch.begin();
        GameRender(Main.batch);
        Main.batch.end();
    //    Gdx.gl.glClearColor(0, 1, 0, 1);
   //     Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
     //   main.batch.begin();
       // main.batch.draw(main.img, 0, 0);
//        main.batch.end();

}
    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public void multitouch(float x,float y,boolean isDownTouch,int pointer){
        for(int i = 0;i < 5;i++){
            joy.update(x,y,isDownTouch,pointer);
            joy2.update(x,y,isDownTouch,pointer);

        }
    }

    public void GameUpdate(){
        player.setDirection(joy.getDir());
        player.update();
        bullgen.update(joy2);
        for(int i = 0; i < bullets.size;i++){
            bullets.get(i).update(); if(bullets.get(i).isOut)bullets.removeIndex(i--);
        }

    }

    public void GameRender(SpriteBatch batch){

        player.draw(batch);
        joy.draw(batch);
        joy2.draw(batch);
        for(int i = 0; i < bullets.size;i++){
            bullets.get(i).draw(batch);
        }



    }

    public void loadActors(){
        joy = new Joystick(Main.joy,Main.joy,new Point2D(Main.WIDTH - ((Main.HEIGHT/3)/2+(Main.HEIGHT/3)/4),(Main.HEIGHT/3)/2+(Main.HEIGHT/3)/4),Main.HEIGHT/4);
        joy2 = new Joystick(Main.joy,Main.joy,new Point2D((Main.HEIGHT/3)/4+Main.HEIGHT/5,(Main.HEIGHT/3)/2+(Main.HEIGHT/3)/4),Main.HEIGHT/4);
        bullets = new Array<Bullet>();
        bullgen = new BulletGenerator();
        player = new Player(Main.actor,new Point2D(Main.WIDTH/2,Main.HEIGHT/2),10,Main.HEIGHT/20,3);




    }




}
