package ru.vzg.packman.Actors;

import com.badlogic.gdx.graphics.Texture;

import ru.vzg.packman.graphicObj.GraphicsObj;
import ru.vzg.packman.tools.Circle;
import ru.vzg.packman.tools.Point2D;

public abstract class Actor extends GraphicsObj {
    public Point2D position;
    public float Speed,R;
    public Circle bounds;
    public Point2D direction;

    public Actor(Texture img,Point2D position,float Speed , float R){
        super(img);
        this.position = new Point2D(position);
        this.Speed = Speed;
        this.R = R;
        bounds = new Circle(position,R);
        direction = new Point2D(0,0);

    }
    public void setDirection(Point2D dir){
        direction = dir;
    }
}
