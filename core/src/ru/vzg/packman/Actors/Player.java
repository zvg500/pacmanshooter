package ru.vzg.packman.Actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import ru.vzg.packman.Main;
import ru.vzg.packman.tools.Point2D;

public class Player extends Actor{

    private int  Score = 0;
    private float health;

    public Player(Texture img, Point2D position, float Speed, float R, float health) {
        super(img, position, Speed, R);
        this.health = health;
    }

    @Override
    public void draw(SpriteBatch batch) {
        batch.draw(img,position.getX(),position.getY());
    }

    @Override
    public void update() {
        if(position.getX()+R > Main.WIDTH)position.setX(Main.WIDTH-R);
        if(position.getX()-R < 0)position.setX(R);
        if(position.getY()+R > Main.HEIGHT)position.setY(Main.HEIGHT-R);
        if(position.getY()-R < 0)position.setY(R);
        position.add(direction.getX()*Speed,direction.getY()*Speed);
    }
}
